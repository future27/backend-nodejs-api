Backend THEMARKET

(OPS DS BSS DEVELOPMENT)

DB start
docker run -p 5432:5432 -e POSTGRES_PASSWORD=passwordDB -e POSTGRES_USER=userDB -e POSTGRES_DB=nameDB postgres:14.1

(was checkoing in DataGrip)

For docs check - http://localhost:port/api/docs#//api/docs#/ or public link.

"Here we code again...


1.21

steps: 
1 - http://localhost:4000/roles POST
    {
    "value": "USER",
    "description": "some user"
    }

2- http://localhost:4000/auth/registration POST
    {
    "email": "post@example.com",
    "password": "here_we23_C"
    }

3 - http://localhost:4000/users GET 
Проверили с токеном админа всех пользователей, чтобы получить UserID!
Получили: 

[
{
"id": 1,
"email": "post@example.com",
"password": "$2a$05$O4Ydmi0eBXO4uI.9U3QD8eJ8JjUWKRiaqIcMXPUu6PtKNJ9URN4nK",
"banned": false,
"banReason": null,
"createdAt": "2022-09-29T21:11:15.107Z",
"updatedAt": "2022-09-29T21:11:15.107Z",
"roles": [
{
"id": 1,
"value": "ADMIN",
"description": "some admin",
"createdAt": "2022-09-29T21:10:39.897Z",
"updatedAt": "2022-09-29T21:10:39.897Z",
"UserRoles": {
"id": 1,
"roleId": 1,
"userId": 1
}
}
],
"posts": []
}
]

Внимание! Постов не найдено! Давайте их создадим!

4 - http://localhost:4000/post POST 
Так же с админским токеном создаем пост с телом 

title: string
content: string
userId: number
image: вложение

5 - http://localhost:4000/users GET
Теперь проверяем GET запросом пользователей и наличие у них ролей и постов

В конечном счете проведены следующие действия с пользователем:
[
{
"id": 1,
"email": "post@example.com",
"password": "$2a$05$O4Ydmi0eBXO4uI.9U3QD8eJ8JjUWKRiaqIcMXPUu6PtKNJ9URN4nK",
"banned": false,
"banReason": null,
"createdAt": "2022-09-29T21:11:15.107Z",
"updatedAt": "2022-09-29T21:11:15.107Z",
"roles": [
{
"id": 1,
"value": "ADMIN",
"description": "some admin",
"createdAt": "2022-09-29T21:10:39.897Z",
"updatedAt": "2022-09-29T21:10:39.897Z",
"UserRoles": {
"id": 1,
"roleId": 1,
"userId": 1
}
},
{
"id": 2,
"value": "USER",
"description": "some user",
"createdAt": "2022-09-29T21:10:51.813Z",
"updatedAt": "2022-09-29T21:10:51.813Z",
"UserRoles": {
"id": 2,
"roleId": 2,
"userId": 1
}
}
],
"posts": [
{
"id": 1,
"title": "wfqwfwq",
"content": "wefqwf",
"image": "f10a29ea-d9ef-4475-8348-60fe36d44363.jpg",
"userId": 1,
"createdAt": "2022-09-29T21:12:26.184Z",
"updatedAt": "2022-09-29T21:12:26.184Z"
}
]
}
]

image.png