import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
// import { GetAllRolesDto } from './dto/get-roles.dto';
// import { Role } from './roles.model';

@ApiTags('Действия с ролями')
@Controller('roles')
export class RolesController {
  constructor(private roleService: RolesService) {}

  @ApiOperation({ summary: 'Create role' })
  @Post()
  create(@Body() dto: CreateRoleDto) {
    return this.roleService.createRole(dto);
  }

  @ApiOperation({ summary: 'Check ones role' })
  @Get(':value')
  getByValue(@Param('value') value: string) {
    return this.roleService.getRoleByValue(value);
  }

  // http://localhost:5001/roles/ADMIN

  @ApiOperation({ summary: 'Get all roles from db' })
  @Post('all_r')
  getAllRoles() {
    return this.roleService.getAllRoles();
  }
}
